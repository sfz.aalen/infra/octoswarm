from flask import Flask, render_template
import json
from flask import jsonify, request
from flask import Response
import os
import logging
from resources.thumbnails import extract_thumbnail
import base64
from flask_oidc import OpenIDConnect

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(level=LOGLEVEL)

uploadkey = os.environ.get('UPLOAD_KEY', 'test123')

app = Flask(__name__)
OIDC_BASE = os.environ.get('OIDC_BASE', 'https://auth.sfz-aalen.space/realms/master')
app.config.update({
    'SECRET_KEY': os.environ.get('FLASK_SECRET_KEY', 'mysecret'),
    'OIDC_REDIRECT_URI': os.environ.get('OIDC_REDIRECT_URI', 'http://localhost:5000/oidc_callback'),
    'OIDC_ISSUER': os.environ.get('OIDC_ISSUER', 'https://auth.sfz-aalen.space/auth/realms/master'),
    'OIDC_SCOPES': ['openid', 'email', 'profile', 'groups'],
    'OIDC_CLIENT_SECRETS': {
        'web': {
            'client_id': os.environ.get('OIDC_CLIENT_ID', 'octoswarm'),
            'client_secret': os.environ.get('OIDC_CLIENT_SECRET', 'your_default_client_secret'),
            "auth_uri": OIDC_BASE+"/protocol/openid-connect/auth",
            "token_uri": OIDC_BASE+"/protocol/openid-connect/token",
            "userinfo_uri": OIDC_BASE+"/protocol/openid-connect/userinfo",
            "issuer": OIDC_BASE
        }
    },
})

oidc = OpenIDConnect(app)

import static

@app.route('/api/version')
def replyversion():
    version = json.loads('{"api": "0.1",  "server": "1.9.2",  "text": "OctoPrint 1.9.2"}')
    return jsonify(version)

@app.route('/')
def approve():
    return render_template('help.html')

@app.route('/backend', methods = ['GET', 'POST'])
@oidc.require_login
def backend():
    if request.method == 'GET':
        files = []
        for i in os.listdir(static.uploaded_dir):
            image = "data:image/png;base64,"+base64.b64encode(extract_thumbnail(os.path.join(static.uploaded_dir, i))).decode()
            body = {"name": i, "image": image}
            files.append(body)
        return render_template('index.html', files=files)
    elif request.method == 'POST':
        if not '/Mentoren' in oidc.user_getinfo('groups')['groups']:
            return Response("Kein Mentor", status=403)
        logging.info("Got request")
        if request.json["answer"] == 1:
            logging.debug("Accepting {}".format(request.json["name"]))
            outfilepath = os.path.join(static.printed_accept_dir, request.json["name"])
            if os.path.exists(outfilepath):
                return Response("File already in queue", status=425)
            else:
                os.rename(os.path.join(static.uploaded_dir, request.json["name"]), outfilepath)
                return Response("File queued", status=200)
        else:
            logging.debug("Discarding {}".format(request.json["name"]))
            outfilepath = os.path.join(static.printed_discard_dir, request.json["name"])
            if os.path.exists(outfilepath):
                os.remove(outfilepath)
            os.rename(os.path.join(static.uploaded_dir, request.json["name"]), outfilepath)
            return Response("File deleted", status=200)

@app.route('/api/files/local', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        if not request.headers["X-Api-Key"] == uploadkey:
            return Response("Falscher Key", status=403)
        if ".gcode" in f.filename:
            uploadfile = os.path.join(static.uploaded_dir, f.filename)
            if os.path.exists(uploadfile):
                os.remove(uploadfile)
            f.save(uploadfile)
            return Response("{}", status=201, mimetype='application/json')
        else:
            return Response("File is no gcode", status=500)
    
app.run(host="0.0.0.0", port=int(os.environ.get('PORT', '5000')))