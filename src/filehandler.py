from resources.octoprint import OctoPrint
import logging, os
import static
import time
import yaml

LOGLEVEL = os.environ.get('LOGLEVEL', 'INFO').upper()
logging.basicConfig(level=LOGLEVEL)

with open(os.environ.get('CONFIGPATH', 'config.yaml'), 'r') as file:
    config = yaml.safe_load(file)

op = OctoPrint(config)

while True:
    for file in os.listdir(static.printed_accept_dir):
        if ".gcode" in file:
            logging.info("Found file {}, processing".format(file))
            printers = op.checkOperational()
            for i in printers:
                printer = printers[i]
                if printer['available'] == 1:
                    op.PrintFile(printer['ip'], printer['port'], printer['api-key'], os.path.join(static.printed_accept_dir, file))
                    logging.info("Uploaded {} to {}:{}".format(file, printer['ip'], printer['port']))
                    break
    time.sleep(10)
    
