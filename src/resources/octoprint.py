import requests
import json
import os 

class OctoPrint:
    def __init__(self, conf):
        self.config = conf

    async def PrintFile(self, ip, port, apikey, filename):
       fle={'file': open(filename, 'rb'), 'filename': '{}.gcode'.format(filename)}
       url='http://{}:{}/api/files/{}'.format(ip,port,'local')
       payload={'select': 'true','print': 'false' }
       header={'X-Api-Key': apikey }
       requests.post(url, files=fle,data=payload,headers=header)
    
    def checkOperational(self):
        def checkPrinter(ip,port,apikey):
            url="http://{}:{}/api/connection".format(ip,port)
            header={'X-Api-Key': '{}'.format(apikey) }
            response = requests.get(url, headers=header)
            if(response.status_code == 200):
                return 1 if json.loads(response.content)['current']['state'] == "Operational" else 0
            else:
                return 0
        printers = self.config["printers"]
        for i in self.config["printers"]:
            printer = printers[i]
            printer["available"] = checkPrinter(printer["ip"],printer["port"],os.environ.get(printer["api-key"], 'sampleapikey'))
        return printers