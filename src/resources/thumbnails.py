# Inspired by https://github.com/jneilliii/OctoPrint-PrusaSlicerThumbnails/blob/master/octoprint_prusaslicerthumbnails/__init__.py
import re
import base64, io
from PIL import Image

def extract_thumbnail(gcode_filename):
	with open(gcode_filename, "r") as gcode_file:
		pattern_start = re.compile(".*thumbnail_QOI begin.*")
		pattern_end = re.compile(".*thumbnail_QOI end.*")
		started = False
		image = []
		imageCount = 0
		for line in gcode_file:
			if pattern_end.match(str(line)):
				started = False
			if started:
				image[imageCount-1] += str(line).replace("; ", "").replace("\\n", "").replace("\\r", "")
				image[imageCount-1] = "".join(l for l in image[imageCount-1].splitlines() if l)
				
			if pattern_start.match(str(line)):
				started = True
				imageCount = imageCount + 1
				image.append("")
		if len(image) > 0:
			image = Image.open(io.BytesIO(base64.b64decode(image[1])), formats=["QOI"])
			with io.BytesIO() as png_bytes:
				image.save(png_bytes, "PNG")
				png_bytes_string = png_bytes.getvalue()
			return png_bytes_string
		else:
			return "".encode()
    