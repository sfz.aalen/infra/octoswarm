import os
import yaml

outdir = os.environ.get('OUTDIR', 'out')
printed_accept_dir = os.path.join(outdir, os.environ.get('OUTDIR_SUBPATH_ACCEPT', "accept"))
printed_discard_dir = os.path.join(outdir, os.environ.get('OUTDIR_SUBPATH_DISCARD', "discard"))
uploaded_dir = os.path.join(outdir, os.environ.get('OUTDIR_SUBPATH_UPLOAD', "upload"))

if not os.path.exists(outdir):
    os.mkdir(outdir)
if not os.path.exists(printed_accept_dir):
    os.mkdir(printed_accept_dir)
if not os.path.exists(printed_discard_dir):
    os.mkdir(printed_discard_dir)
if not os.path.exists(uploaded_dir):
    os.mkdir(uploaded_dir)