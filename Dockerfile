FROM python:latest

COPY src/requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

COPY ./src/ /app/
WORKDIR /app/